from django.conf.urls import include, url
from geosunny import views as geosunny_views

urlpatterns = [
    url(r'^$', geosunny_views.home, name='home'),
    url(r'^about/$', geosunny_views.about, name='about'),

    url(r'^api/', include('darksky.urls')),
]
