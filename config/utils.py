from django.urls import reverse


def reverse_proxy(url_name_or_view, *args, **kwargs):
    """
    Proxies django.urls.reverse() to allow passing the args/kwargs in splat/double-splat style.
    This means that the url_conf, prefix, and current_app kwargs will all be the defaults.
    """
    return reverse(url_name_or_view, args=args, kwargs=kwargs)
