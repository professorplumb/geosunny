import functools
import re
from django.http import JsonResponse
from geosunny.models import ZipCodeLocation


def return_json(view_func):
    @functools.wraps(view_func)
    def wrapper_func(*args, **kwargs):
        json_dict = {'success': True, 'error': None, 'data': {}}
        try:
            resp = view_func(*args, **kwargs)
            if type(resp) not in (list, dict):
                raise ValueError("Function decorated by return_json must return a list or dict")
            json_dict['data'] = resp
        except Exception as e:
            json_dict['success'] = False
            json_dict['error'] = str(e)

        return JsonResponse(json_dict)
    return wrapper_func


def location_from_code(view_func):
    """
    Converts a location code (ZIP code, Google Places API reference, etc.) to a location containing latitude & longitude
    and passes it as the second argument (after the request) to the view func
    """
    @functools.wraps(view_func)
    def wrapper_func(request, *args, **kwargs):
        location_code = kwargs['location_code']
        if re.match(r'\d{5}', location_code):  # assume it's a ZIP code
            zip_obj = ZipCodeLocation.objects.get(zip_code=int(location_code))
        else:
            # no non-ZIP support for now
            raise ValueError("'{}' is not a ZIP code".format(location_code))
        return view_func(request, zip_obj, *args, **kwargs)
    return wrapper_func
