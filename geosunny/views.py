from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request):
    location_code = 94117
    if request.method == 'GET' and 'location_code' in request.GET:
        location_code = int(request.GET['location_code'])
    return render_to_response('home.html',
                              {'init_location_code': location_code, 'init_time_code': "now", })


def about(request):
    return HttpResponse("About six feet.")