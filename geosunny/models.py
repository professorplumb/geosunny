from django.contrib.gis.db import models
from django.contrib.gis.gdal import Envelope

from darksky import api_wrapper

import datetime
import jsonfield
import re


class Location(models.Model):
    point = models.PointField(geography=True)

    class Meta:
        abstract = True

    @classmethod
    def from_code(cls, code):
        # Delegates to correct subclass based on code pattern
        if re.match(r'\d{5}(?:-\d{4})?', code):
            return ZipCodeLocation.objects.get(zip_code=int(code[:5]))
        raise cls.DoesNotExist

    @classmethod
    def all_within_envelope(cls, min_x, min_y, max_x, max_y, only=None):
        locs = []
        for sub in cls.__subclasses__():
            qs = sub.all_within_envelope(min_x, min_y, max_x, max_y)
            if only:
                qs = qs.only(only)
            locs.extend(qs)
        return locs

    @classmethod
    def get_weather_for_list(cls, location_list):
        weathers = LocationWeather.objects.filter(location_code__in=[l.code for l in location_list])
        print("Retrieved {} weathers of which {} are stale".format(len(weathers), len([w for w in weathers if w.is_stale()])))
        return [w for w in weathers if not w.is_stale()]

    @property
    def code(self):
        raise NotImplementedError

    @property
    def latitude(self):
        raise NotImplementedError

    @property
    def longitude(self):
        raise NotImplementedError

    def get_weather(self):
        try:
            weather = LocationWeather.objects.get(location_code=self.code)
            if not weather.is_stale():
                # logger.debug("Getting cached weather for {}".format(self))
                return weather
        except LocationWeather.DoesNotExist:
            pass

        # create new cached LocationWeather object
        # logger.debug("Creating/updating weather object for {}".format(self))
        return LocationWeather.create_or_update(self)


class LocationWeather(models.Model):
    location_code = models.CharField(max_length=100, primary_key=True)
    valid_as_of = models.DateTimeField(auto_now=True)

    _weather = jsonfield.JSONField()  # dict containing raw weather info from API

    @classmethod
    def create_or_update(cls, location_obj):
        # assume that if this weather was preexisting it has already been checked for staleness; update weather dict
        weather_dict = api_wrapper.weather_for_location(location_obj)
        weather, _ = cls.objects.get_or_create(location_code=location_obj.code,
                                               defaults={'_weather': weather_dict, })
        return weather

    def is_stale(self):
        return datetime.datetime.now(self.valid_as_of.tzinfo) - self.valid_as_of > datetime.timedelta(hours=1)

    def json_dict(self, *args, **kwargs):
        dct = api_wrapper.standardize_weather_dict(self._weather, *args, **kwargs)
        dct.update({
            'id': self.location_code,
            'name': self.location_code,  # TODO: neighborhoods
        })
        return dct


# US locations denoted by zip code

class ZipCodeLocation(Location):
    zip_code = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, null=True)
    city = models.CharField('City', max_length=255)
    state = models.CharField('State', max_length=2)
    lat = models.FloatField()
    lon = models.FloatField()

    @classmethod
    def all_within_envelope(cls, min_x, min_y, max_x, max_y, **kwargs):
        envelope = Envelope(min_x, min_y, max_x, max_y)
        return cls.objects.filter(point__intersects=envelope.wkt)

    def __unicode__(self):
        return self.code

    @property
    def code(self):
        return str(self.zip_code)

    @property
    def latitude(self):
        return self.lat

    @property
    def longitude(self):
        return self.lon
