import json
import datetime
import os
import pytz
import requests

from django.conf import settings
from django.templatetags.static import static


API_KEY = os.environ['DARKSKY_API_KEY']
API_URL = "https://api.darksky.net/forecast/{API_KEY}/{LATITUDE},{LONGITUDE}" + \
    "?exclude=minutely,hourly,daily,alerts,flags"


def weather_for_location(location):
    # logger.debug("API call: weather data for {} ({}, {})".format(location, location.latitude, location.longitude))
    return _weather_for_lat_lon(location.latitude, location.longitude)


def _weather_for_lat_lon(lat, lon):
    api_url = API_URL.format(API_KEY=API_KEY, LATITUDE=lat, LONGITUDE=lon)
    resp = requests.get(api_url)
    weather = json.loads(resp.content)
    weather['source'] = 'darksky.net'
    return weather


# Translating results of Darksky.net's API into our standard form
def darksky_api_to_icon_name(s):
    return s


def darksky_api_to_svg_slug(s):
    return {
        'clear-day': 'day-sunny',
        'clear-night': 'night-clear',
        'sleet': 'hail',
        'wind': 'windy',
        'cloudy': 'cloud',
        'partly-cloudy-day': 'day-cloudy',
        'partly-cloudy-night': 'night-cloudy',
    }.get(s, s)


def standardize_weather_dict(w, info_window=False, *args, **kwargs):
    dct = {
        'latitude': w['latitude'],
        'longitude': w['longitude'],
    }

    if 'currently' in w:
        c = w['currently']
        use_icons = settings.USE_ICONS
        icon_url = static('images/icons/weather/1/{}.png'.format(darksky_api_to_icon_name(c['icon'])))
        dct.update({
            'weather_slug': darksky_api_to_svg_slug(c['icon']),
            'time_observed': datetime.datetime.fromtimestamp(
                c['time'],
                tz=pytz.timezone(w['timezone'])).strftime('%B %d %H:%M %p %Z'),
            'use_icons': use_icons,
            'icon_url': icon_url if use_icons else static('images/1x1.png'),
            'icon_width': 50 if use_icons else 1,
            'icon_height': 50 if use_icons else 1,
        })

        if info_window:
            dct.update({
                'weather': c['summary'],
                'temp': c['temperature'],
                'feels_like': c['apparentTemperature'],
            })

    return dct
