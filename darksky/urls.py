from django.conf.urls import re_path
from darksky import views as api_views

app_name = 'api'
urlpatterns = [
   re_path(r'^map-options/(?P<location_code>[\d\w]+)/$', api_views.map_options,
           name='map_options'),
   re_path(r'^map-options/(?P<location_code>[\d\w]+)/(?P<radius_in_miles>\d+)/$', api_views.map_options,
           name='map_options'),
   re_path(r'^weather-markers/$', api_views.weather_markers, name='weather_markers'),
   re_path(r'^info-window/$', api_views.info_window, name='info_window'),
]
