import threading
import time

from django.conf import settings
from django.template import Context
from django.template.loader import get_template
from config.decorators import location_from_code, return_json
from geosunny.models import Location


@location_from_code
@return_json
def map_options(request, location, location_code='[None]'):
    latitude, longitude = location.latitude, location.longitude
    return {
        'code': "{},{}".format(latitude, location),           # can we remove this?
        'name': location_code,
        'latitude': latitude,
        'longitude': longitude,
        'zoomLevel': 14,
    }


def _get_weather_into_queue(loc, lst):
    weather_data = loc.get_weather()
    lst.append(weather_data)


@return_json
def weather_markers(request):
    min_x, min_y = request.GET['sw'].split(',')
    max_x, max_y = request.GET['ne'].split(',')
    old_sw, old_ne = request.GET.get('old_sw'), request.GET.get('old_ne')

    nearby_points = Location.all_within_envelope(min_x, min_y, max_x, max_y, only='pk')
    if old_sw and old_ne:
        # do not spend time looking up the markers which were already displayed on the old map
        already_displayed = Location.all_within_envelope(*(old_sw.split(',') + (old_ne.split(','))), only='pk')
        nearby_points = [p for p in nearby_points if p not in already_displayed]

    # TODO: if someone requests a large number of points, sample by distance instead of just clamping list
    nearby_points = nearby_points[:50]

    # pull all cached weather objects at once
    weather_list = list(Location.get_weather_for_list(nearby_points))

    # and look up those which are not yet cached via an API call
    look_em_up = [point for point in nearby_points if point.code not in [w.location_code for w in weather_list]]

    # get weather results in parallel as much as is possible
    for point in look_em_up:
        threading.Thread(target=_get_weather_into_queue, args=(point, weather_list)).start()
    start_time = time.time()
    while len(weather_list) < len(look_em_up) \
            and time.time() - start_time < 30:  # 30s timeout
        time.sleep(0.1)

    return {'markers': [w.json_dict() for w in weather_list]}


@return_json
def info_window(request):
    location_code, time_code = request.GET['location_code'], request.GET['time_code']

    # TODO: expand to deal with more than ZIP codes
    loc = Location.from_code(location_code)

    weather_dict = loc.get_weather().json_dict(info_window=True)
    ctx = {
        'weather_dict': weather_dict,
        'more_info_url': 'https://darksky.net/forecast/{latitude},{longitude}/us12/en'.format(**weather_dict),
        'use_icons': settings.USE_ICONS,
    }

    return {'html': get_template('info-window.html').render(ctx)}
