google.maps.visualRefresh = true;
var map, marker_buffer = [];  // marker_buffer currently unused - can remove this if unneeded
function initialize() {
    var map_options = null;
    jQuery.ajax(api_map_options_url, {
        async: false,
        success: function(response) {
            if (response.error) {
                console.log("Error calling " + api_map_options_url + ":");
                console.log(response);
            }
            map_options = create_map_options(response.data);
        }
    });

    map = set_map(map_options);
    google.maps.event.addListener(map, 'idle', update_map);
}

function create_map_options(map_data) {
    return {
        map_name: map_data.name,
        center: new google.maps.LatLng(map_data.latitude, map_data.longitude),
        zoom: map_data.zoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
}

function set_map(options) {
    return new google.maps.Map(document.getElementById("map-canvas"), options)
}

function set_markers(response) {
    if (response.error) {
        console.log("Error getting weather markers:");
        console.log(response);
    }
//    if (marker_buffer.length > 0) {
//        remove_markers();
//    }
    add_markers(response.data.markers);
    jQuery('#mask').hide();
}

function add_markers(marker_list) {
    var i, marker_data, marker, opts;
    for (i=0; i<marker_list.length; i++) {
        marker_data = marker_list[i];
        opts = {
            position: new google.maps.LatLng(marker_data.latitude, marker_data.longitude),
            map: map,
            title: '', //marker_data.place_name,
            icon: new google.maps.MarkerImage(marker_data['icon_url'],
                null, null, null,
                new google.maps.Size(marker_data['icon_width'], marker_data['icon_height'])
            ),
            // below parameters are for our purposes, not for gmaps
            location_code: marker_data.id,
            time_code: marker_data['time_observed']
        };
        if (!marker_data['use_icons']) {
            $.extend(opts, {
                labelClass: 'wi wi-' + marker_data['weather_slug'],
                labelContent: '',
                labelStyle: { 'font-size': '40px' }
            })
        }
        marker_buffer.push(marker = new (marker_data['use_icons'] ? google.maps.Marker : MarkerWithLabel)(opts));
        google.maps.event.addListener(marker, 'click', (function(m) {
            return function() {
                var iw = new google.maps.InfoWindow({
                    content: '<img src=\"' + loading_image_url + '\" />'
                });
                m.info_window = iw;
                jQuery.ajax(api_info_window_url + '?location_code=' + m.location_code + '&time_code=' + m.time_code, {
                    success: function(response, status, xhr) {
                        iw.setContent(response.data.html);
                    }
                });
                iw.open(map, m)
            }
        })(marker));
    }
}

//function remove_markers() {
//    for (var i=0; i<marker_buffer.length; i++) {
//        delete(marker_buffer[i].info_window);
//        google.maps.event.clearListeners(marker_buffer[i], 'click');
//        marker_buffer[i].setMap(null);
//    }
//    marker_buffer = []
//}

function update_map() {
    var bounds = map.getBounds(),
        sw = bounds.getSouthWest(),
        ne = bounds.getNorthEast(),
        marker_url = api_weather_markers_url,
        old_sw, old_ne;

    marker_url += "?sw=" + sw.lng() + "," + sw.lat();
    marker_url += "&ne=" + ne.lng() + "," + ne.lat();
    if (map._oldBounds) {
        old_sw = map._oldBounds.getSouthWest();
        old_ne = map._oldBounds.getNorthEast();
        marker_url += "&old_sw=" + old_sw.lng() + "," + old_sw.lat();
        marker_url += "&old_ne=" + old_ne.lng() + "," + old_ne.lat();
    }
    map._oldBounds = bounds;
    jQuery.ajax(marker_url, {
        success: set_markers
    });
}

function validate_location_code(form) {
    var location_code = form.elements['location_code'].value,
        zip_validator = /^\d{5}$/;  // currently only validating 5-digit ZIPs
    if (!zip_validator.test(location_code)) {
        alert("Please enter a 5-digit US ZIP!");
        return false;
    }
    return true;
}

google.maps.event.addDomListener(window, 'load', initialize);
